# Twinkit

## Description
gRPC client-server streaming example using microservices approach.

Coded in `Go`. Running in `Docker`. Using `bash` script for launch automation.

## Installation
You need to install `Docker` and `docker-compose` to run project.

## Usage
To start project exec bash script:
```
./launcher.sh start
```

It will download all needed Docker images using docker-compose, run needed Docker containers and run demo.

Check `./launcher.sh help` for more options.