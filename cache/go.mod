module gitlab.com/sergeysynergy-go/twinkit/cache

go 1.16

require (
	github.com/go-redis/redis/v8 v8.11.3
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.25.0
	github.com/spf13/viper v1.9.0
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1
)

replace gitlab.com/sergeysynergy-go/twinkit/cache => ../cache
