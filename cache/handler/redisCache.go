package handler

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"math/rand"
	"sync"
	"time"

	pb "gitlab.com/sergeysynergy-go/twinkit/cache/proto"
)

type dataFetcher interface {
	// GetData return data to store in cache.
	GetData(string) ([]byte, error)
}

type redisCacheJob struct {
	cache 		*RedisCache
	jobNumber 	int
	address		string
	out 		chan<- *pb.Response
	data 		[]byte
	logPrefix 	string
}

type RedisCache struct {
	Rdb 			*redis.Client
	Dtf				dataFetcher
	MaxLockTimeout 	int
	MinTimeout 		int
	MaxTimeout 		int
	URLs 			[]string
	mu     			sync.RWMutex
	jobsNumber 		int
}

func (j *redisCacheJob) set(ctx context.Context) error {
	t := time.Duration(rand.Intn(j.cache.MaxTimeout - j.cache.MinTimeout) + j.cache.MinTimeout)
	err := j.cache.Rdb.Set(ctx, j.address, j.data, t*time.Second).Err()
	if err != nil {
		return errors.Wrap(err, "redis set key error")
	}
	log.Debug().Msgf("%s data set successful", j.logPrefix)
	return nil
}

func (j *redisCacheJob) lockRebuild(ctx context.Context) error {
	lockKey := "lock_" + j.address
	lockAcquired := false
	for i := 0; i < j.cache.MaxLockTimeout; i++ {
		stored := j.cache.Rdb.SetNX(ctx, lockKey, []byte("1"), time.Duration(j.cache.MaxLockTimeout)*time.Second).Val()
		if stored != true {
			log.Debug().Msgf("%s get lock try %d", j.logPrefix, i)
			time.Sleep(time.Second)  // Sleeping second to fit in max lock timeout period.
			continue
		}
		lockAcquired = true
		log.Debug().Msgf("%s lock successful", j.logPrefix)
		break
	}
	if !lockAcquired {
		return fmt.Errorf("can’t get lock")
	}
	return nil
}

func (j *redisCacheJob) unlockRebuild(ctx context.Context) error {
	lockKey := "lock_" + j.address
	err := j.cache.Rdb.Del(ctx, lockKey).Err()
	if err != nil {
		return errors.Wrap(err, "redis delete key error")
	}
	log.Debug().Msgf("%s unlock rebuild successful", j.logPrefix)
	return nil
}

func (j *redisCacheJob) rebuild(ctx context.Context) error {
	log.Debug().Msgf("%s rebuilding cache", j.logPrefix)
	err := j.lockRebuild(ctx)
	if err != nil {
		return errors.Wrap(err, "lock rebuild failed")
	}
	defer j.unlockRebuild(ctx)

	// Need another cache existence check after lock wait.
	val, err := j.cache.Rdb.Get(ctx, j.address).Result()
	if err == redis.Nil {
		log.Debug().Msgf("%s key check after getting lock: key not found in redis", j.logPrefix)

		log.Debug().Msgf("%s start fetching remote data", j.logPrefix)
		j.data, err = j.cache.Dtf.GetData(j.address)
		if err != nil {
			return err
		}
		log.Debug().Msgf("%s end fetching remote data", j.logPrefix)

		log.Info().Msgf("%s FINISHED request, data loaded remotely", j.logPrefix)
		err = j.set(ctx)
		if err != nil {
			return err
		}
		return nil
	} else if err != nil {
		return err
	} else {
		// Get data from cache if cache just has been rebuild.
		log.Info().Msgf("%s FINISHED request, data loaded from redis", j.logPrefix)
		j.data = []byte(val)
	}
	return nil
}

func (j *redisCacheJob) checkLock(ctx context.Context) error {
	lockKey := "lock_" + j.address
	// Set max waiting time to 100 seconds - this is how much it takes to load the most resource-intensive URL.
	for i := 0; i < j.cache.MaxLockTimeout; i++ {
		_, err := j.cache.Rdb.Get(ctx, lockKey).Result()
		if err == redis.Nil {
			return nil
		}
		if err != nil {
			return errors.Wrap(err, "Redis error")
		}
		log.Debug().Msgf("%s check lock try %d", j.logPrefix, i)
		time.Sleep(time.Second)  // Sleeping second to fit in max lock timeout period.
	}
	log.Info().Msgf("%s count resource as unlocked", j.logPrefix)
	return nil
}

func (j *redisCacheJob) handleRequest(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()

	val, err := j.cache.Rdb.Get(ctx, j.address).Result()
	if err == redis.Nil {
		log.Debug().Msgf("%s key not found in redis", j.logPrefix)
		// Rebuild cache if it is not found.
		err = j.rebuild(ctx)
		if err != nil {
			log.Error().Msgf( "%s %v", j.logPrefix, err)
			j.out <- &pb.Response{
				Error: err.Error(),
				URL: j.address,
			}
			return
		}
	} else if err != nil {
		log.Error().Msgf( "%s %v", j.logPrefix, err)
		j.out <- &pb.Response{
			Error: err.Error(),
			URL: j.address,
		}
		return
	} else {
		// Just get data from cache if it is exists.
		j.data = []byte(val)
		log.Info().Msgf("%s FINISHED request, data loaded from redis", j.logPrefix)
	}

	j.out <- &pb.Response{
		Data: 	string(j.data),
		Len:  	int64(len(j.data)),
		URL: 	j.address,
		Error: 	"",
	}
}

func (rc *RedisCache) GetJobsNumber() int {
	return rc.jobsNumber
}

func (rc *RedisCache) increaseJobsNumber() int {
	rc.mu.Lock()
	rc.jobsNumber += 1
	jobNum := rc.jobsNumber
	rc.mu.Unlock()
	return jobNum
}

func (rc *RedisCache) HandleRequest(reqNum, nor int, out chan<- *pb.Response, quit chan<- struct{})  {
	wg := &sync.WaitGroup{}
	for i := 0; i < nor; i++ {
		jobNum := rc.increaseJobsNumber()
		address := rc.URLs[rand.Intn(len(rc.URLs))]
		job := redisCacheJob{
			cache: 		rc,
			jobNumber: 	jobNum,
			address: 	address,
			out: 		out,
			logPrefix: 	fmt.Sprintf("REQ %04d JOB %04d URL %s", reqNum, jobNum, address),
		}
		ctx := context.Background()
		wg.Add(1)
		go job.handleRequest(ctx, wg)
	}
	wg.Wait()
	quit <- struct{}{}
}
