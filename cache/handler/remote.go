package handler

import (
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
	"net/url"
)

type RemoteData struct {
	Client 	*http.Client
}

func (rd *RemoteData) GetData(address string) ([]byte, error) {
	req := &http.Request{
		Method: http.MethodGet,
		Header: http.Header{
			"User-Agent": {"twinkit/cache"},
		},
	}
	req.URL, _ = url.Parse(address)
	resp, err := rd.Client.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "remote resource error")
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "reading body error")
	}
	return respBody, nil
}
