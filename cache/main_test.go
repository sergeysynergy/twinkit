package main

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/sergeysynergy-go/twinkit/cache/handler"
	pb "gitlab.com/sergeysynergy-go/twinkit/cache/proto"
	"google.golang.org/grpc"
	"math/rand"
	"net"
	"strconv"
	"testing"
	"time"
)

func TestProtobufStreaming(T *testing.T) {
	flags()
	config()
	rand.Seed(time.Now().UTC().UnixNano())
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	// Init simple cache handler to use with cacheServer.
	simpleCache := &handler.SimpleCache{
	}

	// Init and register cacheServer.
	cacheServer := &cacheServer{
		MaxRequests: 	maxRequestsNumber,
		Cache: 			simpleCache,  // handler to use for caching data
	}
	server := grpc.NewServer()
	pb.RegisterCacheServer(server, cacheServer)

	port := ":" + strconv.Itoa(portNumber)
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatal().Msgf("cant listen port %v", err)
	}
	log.Info().Msgf("starting server at %s", port)
	err = server.Serve(lis)
	if err != nil {
		log.Fatal().Msgf("starting server error %v", err)
	}
}