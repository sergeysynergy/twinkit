package main

import (
	"fmt"
	"github.com/rs/zerolog/log"
	pb "gitlab.com/sergeysynergy-go/twinkit/cache/proto"
	"sync"
)

type cacher interface {
	HandleRequest(int, int, chan<- *pb.Response, chan<- struct{})
}

type cacheServer struct {
	MaxRequests 	int
	Rsp 			pb.Response
	Cache 			cacher
	mu       		sync.RWMutex
	requestsNumber 	int
}

func (srv *cacheServer) increaseRequestsNumber() int {
	srv.mu.Lock()
	srv.requestsNumber += 1
	reqNum := srv.requestsNumber
	srv.mu.Unlock()
	return reqNum
}

func (srv *cacheServer) handleRequest(wg *sync.WaitGroup, stream pb.Cache_GetRandomDataStreamServer, reqNum int) {
	defer wg.Done()
	resultCh := make(chan *pb.Response, 1)
	quitCh := make(chan struct{}, 1)

	go srv.Cache.HandleRequest(
		reqNum,  // number of requests done to server
		numberOfRequests,  // number of request to do inside cache handler
		resultCh,
		quitCh,
	)

	LOOP:
	for {
		select {
		case rsp := <-resultCh:
			if rsp.Error != "" {
				log.Error().Msg(rsp.Error)
			}
			err := stream.Send(rsp)
			if err != nil {
				log.Error().Msgf("server error: %v", err)
			}
		case quit := <-quitCh:
			if &quit != nil {
				fmt.Println("exit")
				break LOOP
			}
		}
	}
	close(resultCh)
	close(quitCh)
	log.Info().Msgf("REQ %04d request finished", reqNum)
}

func (srv *cacheServer) GetRandomDataStream(_ *pb.Request, stream pb.Cache_GetRandomDataStreamServer) error {
	reqNum := srv.increaseRequestsNumber()
	log.Info().Msgf("REQ %04d request started", reqNum)
	if reqNum > srv.MaxRequests {
		err := fmt.Errorf("REQ %04d maximum number of requests exceeded", reqNum)
		log.Fatal().Msg(err.Error())
		return err
	}

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go srv.handleRequest(wg, stream, reqNum)
	wg.Wait()
	return nil
}
