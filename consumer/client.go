package main

import (
	"bytes"
	"context"
	"flag"
	"google.golang.org/grpc"
	"io"
	"sync"

	"github.com/spf13/viper"
	pb "gitlab.com/sergeysynergy-go/twinkit/cache/proto"
	"log"
	"path/filepath"
)

var (
	clientRequestsNumber int
	configPath, server string
)

func flags() {
	flag.StringVar(&configPath, "config", "", "path to configuration file")
	flag.Parse()
}

func config() {
	// Default config
	var yamlConf = []byte(`
ClientRequestsNumber: 	2
Server: 				"localhost:8082"
`)
	viper.SetConfigType("yaml")
	err := viper.ReadConfig(bytes.NewBuffer(yamlConf))
	if err != nil {
		log.Fatal(err)
	}
	viper.SetConfigName(filepath.Base(configPath))
	viper.AddConfigPath(filepath.Dir(configPath))
	err = viper.MergeInConfig()
	if err != nil {
		log.Println("config file not found, using default settings")
	}

	clientRequestsNumber = viper.GetInt("ClientRequestsNumber")
	server = viper.GetString("Server")

	if clientRequestsNumber <= 0 {
		log.Println("wrong numberOfRequests number, set to default value")
		clientRequestsNumber = 1000
	}
}

func main()  {
	flags()
	config()

	grcpConn, err := grpc.Dial(
		server,
		grpc.WithInsecure(),
	)
	if err != nil {
		log.Fatalf("can't connect to grpc server: %v", err)
	}
	defer grcpConn.Close()

	wg := &sync.WaitGroup{}
	for i := 0; i < clientRequestsNumber; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			client := pb.NewCacheClient(grcpConn)
			ctx := context.Background()
			stream, err := client.GetRandomDataStream(ctx, &pb.Request{})
			if err != nil {
				log.Fatal(err)
			}

			for {
				resp, err := stream.Recv()
				if err == io.EOF {
					log.Println("end of stream")
					return
				}
				if err != nil {
					log.Println("server error: ", err)
					return
				}
				if resp.Error != "" {
					log.Println("response error: URL", resp.URL, "-", resp.Error)
					return
				}
				log.Println("response successful: URL", resp.URL, "- data length", resp.Len)
			}
		}()
	}
	wg.Wait()
}