module gitlab.com/sergeysynergy-go/twinkit/consumer

go 1.16

require (
	github.com/spf13/viper v1.9.0
	gitlab.com/sergeysynergy-go/twinkit/cache v0.0.0-00010101000000-000000000000
	google.golang.org/grpc v1.41.0
)

replace gitlab.com/sergeysynergy-go/twinkit/cache => ../cache
