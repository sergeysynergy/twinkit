# Stage 1: building executable file
FROM golang:1.16.8 as build-stage
COPY ./pager /workdir/pager
COPY ./cache /workdir/cache
WORKDIR /workdir/cache
RUN go mod edit -replace gitlab.com/sergeysynergy/twinkit/pager=../pager
RUN go mod tidy
RUN go build .

# Stage 2: copy just executable file and config ready for production
FROM golang:1.16.8
COPY --from=build-stage /workdir/cache/cache /usr/local/bin/.
COPY ./env/cache-local-conf.yml /go/local-conf.yml

CMD ["cache", "-config", "/go/local-conf.yml"]
