#!/bin/bash

help() {
  cat << EOF
This is a tool for managing gRPC client-server streaming demo using microservices approach.
Coded in Go. Running in Docker.


Usage:
  ./launcher.sh [command]

Available Commands:
  help        - this help
  start       - build and start demo
  stop        - stop Docker containers
  rebuild     - rebuild Docker containers
  purge       - delete all built Docker containers

EOF
}

start() {
  cd ./env
  docker-compose up -d
  cd ..
  echo "making requests from client to server container, watching server logs:"
  docker exec twinkit_consumer consumer -config=/go/local-conf.yml
  docker logs twinkit_cache
}

stop() {
  cd ./env
  docker-compose stop
  cd ..
}

if [[ $1 = "help" ]]; then
  help
  exit 0
fi

if [[ $1 = "start" ]]; then
  start
  exit 0
fi

if [[ $1 = "stop" ]]; then
  stop
  exit 0
fi

if [[ $1 = "rebuild" ]]; then
  clear
  echo "DO NOT run this command in parallel with other docker operations!!!"
  echo "clearing docker-cache..."
  echo "--------------------------------------------------------"
  docker stop twinkit_consumer twinkit_cache twinkit_redis
  docker rmi $(docker images -a --filter=dangling=true -q)
  echo "rebuilding..."
  cd ./env
  docker-compose build
  cd ..
  exit 0
fi

if [[ $1 = "purge" ]]; then
  docker stop twinkit_consumer twinkit_cache twinkit_redis
  docker rm twinkit_consumer twinkit_cache twinkit_redis
  exit 0
fi

help